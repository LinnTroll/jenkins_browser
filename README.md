# README #

Start project:

```
python manage.py syncdb
python manage.py migrate
python manage.py runserver
```

Periodic fetching (run in cron every 5 minutes):

```
python manage.py fetch
```