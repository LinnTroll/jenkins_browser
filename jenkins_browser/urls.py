from django.conf.urls import url
from django.contrib import admin

from core.views import (
    JobListView,
    JobView,
    BuildTestsView,
    JobTestsView,
    ReposListView,
    RepoTestsView,
    TestPassagesView,
)

urlpatterns = [
    url(r'^$', JobListView.as_view(), name='core_job_list'),
    url(r'^job/(?P<name>[^/]+)/$', JobView.as_view(), name='core_job'),
    url(r'^job/(?P<name>[^/]+)/(?P<number>[^/]+)/tests/$', BuildTestsView.as_view(), name='core_build_tests'),
    url(r'^job/(?P<name>[^/]+)/(?P<number>[^/]+)/diff/$', BuildTestsView.as_view(diff=True), name='core_build_diff'),
    url(r'^job/(?P<name>[^/]+)/tests/$', JobTestsView.as_view(), name='core_job_tests'),
    url(r'^job/(?P<name>[^/]+)/tests/(?P<page>[^/]+)/$', JobTestsView.as_view(), name='core_job_tests'),
    url(r'^job/(?P<name>[^/]+)/tests/(?P<page>[^/]+)/(?P<size>[^/]+)/$', JobTestsView.as_view(), name='core_job_tests'),

    url(r'^repos/$', ReposListView.as_view(), name='repos_list'),
    url(r'^repo/(?P<name>[^/]+)/tests/$', RepoTestsView.as_view(), name='repo_tests'),
    url(r'^repo/(?P<name>[^/]+)/test/(?P<test>[^/]+)/passages/$', TestPassagesView.as_view(), name='test_passages'),

    url(r'^admin/', admin.site.urls),
]
