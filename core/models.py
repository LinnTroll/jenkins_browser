from __future__ import unicode_literals

import os
import re
import json
import hashlib
import datetime
from urlparse import urlparse
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from core.utils import json_defaults


class Load(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    empty = models.BooleanField(default=True)

    def __unicode__(self):
        return 'Load {}'.format(self.id)


class LoadItem(models.Model):
    TYPE_CHOICES = (
        ('create', 'Create'),
        ('update', 'Update'),
    )
    load = models.ForeignKey(Load)
    load_type = models.CharField(max_length=10, choices=TYPE_CHOICES)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.CharField(max_length=500)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return '{} {} {}'.format(self.get_load_type_display(), self.content_type, self.content_object)


class JUser(models.Model):
    name = models.CharField(max_length=100, primary_key=True)

    def __unicode__(self):
        return self.get_display_name()

    def get_display_name(self):
        if self.name and '@' in self.name:
            return self.name.split('@')[0]
        return self.name


class Node(models.Model):
    name = models.CharField(max_length=250, primary_key=True)

    def __unicode__(self):
        return self.name


class Repo(models.Model):
    name = models.CharField(max_length=500, primary_key=True)
    def_job = models.ForeignKey('Job', blank=True, null=True)

    def __unicode__(self):
        return self.get_display_name()

    def get_display_name(self):
        parsed = urlparse(self.name)
        path = parsed.path.strip('/')
        return os.path.splitext(path)[0]

    @property
    def display_name(self):
        return self.get_display_name()

    def get_unique_tests(self):
        tests = Test.objects.filter(build__repo=self).values('name').distinct()
        return sorted([x['name'] for x in tests])

    def get_tests_count(self):
        return len(self.get_unique_tests())

    @property
    def tests_count(self):
        return self.get_tests_count()


class Branch(models.Model):
    repo = models.ForeignKey(Repo)
    name = models.CharField(max_length=500)

    class Meta:
        unique_together = (('repo', 'name'),)

    def __unicode__(self):
        return self.get_branch_name() or ''

    def get_branch_name(self):
        if self.name:
            return os.path.split(self.name)[1]

    def task_number(self):
        return self.get_task_number() or ''

    def get_task_number(self):
        digits = re.sub(r'[^\d]+', '_', self.name).strip('_').split('_')
        if digits:
            return u'/'.join(digits)


class Job(models.Model):
    name = models.CharField(max_length=250, primary_key=True)
    number = models.IntegerField(null=True, blank=True)
    exists = models.BooleanField(default=True)

    class Meta:
        ordering = ('number',)

    def __unicode__(self):
        return self.name

    def get_last_build(self):
        return self.build_set.order_by('-number').first()

    def get_repo(self):
        last_build = self.get_last_build()
        if last_build:
            return last_build.repo

    def get_branch(self):
        last_build = self.get_last_build()
        if last_build:
            return last_build.branch

    def get_juser(self):
        last_build = self.get_last_build()
        if last_build:
            return last_build.juser

    def save(self, *args, **kwargs):
        digits = re.sub(r'[^\d]+', '_', self.name).strip('_').split('_', 1)
        if len(digits) > 0:
            try:
                self.number = int(digits[0])
            except (TypeError, ValueError):
                pass
        super(Job, self).save(*args, **kwargs)


class Build(models.Model):
    job = models.ForeignKey(Job)
    url = models.CharField(max_length=1000, null=True, blank=True)
    number = models.PositiveIntegerField()
    is_building = models.BooleanField(default=False)
    status = models.CharField(max_length=32, null=True, blank=True)
    revision = models.CharField(max_length=40, null=True, blank=True)
    node = models.ForeignKey(Node, null=True, blank=True)
    repo = models.ForeignKey(Repo, null=True, blank=True)
    branch = models.ForeignKey(Branch, null=True, blank=True)
    juser = models.ForeignKey(JUser, null=True, blank=True)
    build_at = models.DateTimeField(blank=True, null=True)
    state_hash = models.CharField(max_length=32)
    tests_fetched = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=datetime.datetime.now)

    class Meta:
        ordering = ('number',)
        unique_together = (('job', 'number'),)

    def __unicode__(self):
        return '{} #{}'.format(self.job_id, self.number)

    @property
    def short_revision(self):
        return self.get_short_revision() or ''

    def get_short_revision(self):
        if self.revision:
            return self.revision[:7]

    def save(self, *args, **kwargs):
        dump = {
            'url': self.url,
            'build_at': self.build_at,
            'job_name': self.job_id,
            'number': self.number,
            'is_building': self.is_building,
            'node': self.node_id,
            'status': self.status,
            'repo': self.repo_id,
            'branch': getattr(self.branch, 'name', None),
            'revision': self.revision,
            'juser': self.juser_id,
        }
        self.state_hash = hashlib.md5(json.dumps(dump, sort_keys=True, default=json_defaults)).hexdigest()
        super(Build, self).save(*args, **kwargs)

    def get_tests_count(self, status=None):
        if status:
            return self.test_set.filter(status=status).count()
        return self.test_set.count()

    @property
    def tests_passed_count(self):
        return self.get_tests_count(status='PASSED')

    @property
    def tests_skipped_count(self):
        return self.get_tests_count(status='SKIPPED')

    @property
    def tests_failed_count(self):
        return self.get_tests_count(status='FAILED')

    @property
    def tests_regression_count(self):
        return self.get_tests_count(status='REGRESSION')

    @property
    def tests_fixed_count(self):
        return self.get_tests_count(status='FIXED')

    @property
    def tests_count(self):
        return self.get_tests_count(status='')


class Test(models.Model):
    build = models.ForeignKey(Build)
    name = models.CharField(max_length=500)
    status = models.CharField(max_length=32)

    def __unicode__(self):
        return '{} {}'.format(self.name, self.status)
