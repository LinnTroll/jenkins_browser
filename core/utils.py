import datetime


def json_defaults(item):
    if isinstance(item, datetime.datetime):
        return item.isoformat()
    return item
