from django.contrib import admin
from models import Job, Build, Node, Branch, Repo, JUser, Load, LoadItem, Test


class JobAdmin(admin.ModelAdmin):
    list_display = ('name', 'number')
    list_filter = ('number',)


admin.site.register(Job, JobAdmin)


class BuildAdmin(admin.ModelAdmin):
    list_display = ('number', 'job', 'url', 'is_building', 'status', 'node', 'juser', 'repo', 'branch',
                    'build_at', 'tests_fetched')
    list_filter = ('is_building', 'tests_fetched', 'status', 'node', 'repo', 'juser')


admin.site.register(Build, BuildAdmin)


class NodeAdmin(admin.ModelAdmin):
    list_display = ('name',)


admin.site.register(Node, NodeAdmin)


class RepoAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_display_name', 'tests_count', 'def_job')


admin.site.register(Repo, RepoAdmin)


class BranchAdmin(admin.ModelAdmin):
    list_display = ('name', 'repo')
    list_filter = ('repo',)


admin.site.register(Branch, BranchAdmin)


class JUserAdmin(admin.ModelAdmin):
    list_display = ('name',)


admin.site.register(JUser, JUserAdmin)


class LoadItemInline(admin.TabularInline):
    model = LoadItem
    extra = 0
    max_num = 0
    can_delete = False
    readonly_fields = ('load_type', 'content_type', 'object_id')


class LoadAdmin(admin.ModelAdmin):
    def statistic(self, item):
        result = []
        created = item.loaditem_set.filter(load_type='create').count()
        updated = item.loaditem_set.filter(load_type='update').count()
        if created:
            result.append((u'Created', created))
        if updated:
            result.append((u'Updated', updated))
        return u', '.join([u'{}: {}'.format(k, v) for k, v in result])

    list_display = ('__unicode__', 'created_at', 'statistic', 'empty')
    list_filter = ('empty',)
    inlines = (LoadItemInline,)


admin.site.register(Load, LoadAdmin)


class LoadItemAdmin(admin.ModelAdmin):
    list_display = ('load', 'load_type', 'content_type', 'content_object')


admin.site.register(LoadItem, LoadItemAdmin)


class TestAdmin(admin.ModelAdmin):
    list_display = ('name', 'build', 'status')
    list_filter = ('status', )


admin.site.register(Test, TestAdmin)
