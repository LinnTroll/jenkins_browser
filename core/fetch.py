import json
import hashlib
import datetime
from requests.exceptions import HTTPError
from django.utils import timezone
from django.conf import settings
from jenkinsapi.jenkins import Jenkins
from jenkinsapi.jenkinsbase import JenkinsBase
from jenkinsapi.build import Build as JenkinsBuild
from jenkinsapi.result_set import ResultSet
from core.models import Job, Build, JUser, Repo, Branch, Node, Load, LoadItem, Test
from core.utils import json_defaults

jenkins = Jenkins(
    settings.JENKINS_URL,
    username=settings.JENKINS_USERNAME,
    password=settings.JENKINS_PASSWORD,
)


class FakeBuild(JenkinsBuild):
    def __init__(self, url):
        self._data = {'url': url}


class SimpleResultSet(ResultSet):
    def __init__(self, url):
        JenkinsBase.__init__(self, url)

    def __str__(self):
        return 'Test Result'

    def get_jenkins_obj(self):
        return jenkins


def sync_jusers(data):
    jusers = [x for x in set([x['juser'] for x in data]) if x]
    exists_jusers = [x['name'] for x in JUser.objects.values('name')]
    new_jusers = set(jusers) - set(exists_jusers)
    for juser_name in new_jusers:
        juser = JUser(name=juser_name)
        juser.save()
        yield juser


def sync_nodes(data):
    nodes = [x for x in set([x['node'] for x in data]) if x]
    exists_nodes = [x['name'] for x in Node.objects.values('name')]
    new_nodes = set(nodes) - set(exists_nodes)
    for node_name in new_nodes:
        node = Node(name=node_name)
        node.save()
        yield node


def sync_repos(data):
    repos = [x for x in set([x['repo'] for x in data]) if x]
    exists_repos = [x['name'] for x in Repo.objects.values('name')]
    new_repos = set(repos) - set(exists_repos)
    for repo_name in new_repos:
        repo = Repo(name=repo_name)
        repo.save()
        yield repo


def sync_branches(data):
    branches = [x for x in set([(x['repo'], x['branch']) for x in data]) if x[0] and x[1]]
    exists_branches = [(x['repo_id'], x['name']) for x in Branch.objects.values('repo_id', 'name')]
    new_branches = set(branches) - set(exists_branches)
    for repo_name, branch_name in new_branches:
        branch = Branch(
            repo_id=repo_name,
            name=branch_name,
        )
        branch.save()
        yield branch


def sync_jobs(data):
    jobs = [x for x in set([x['job_name'] for x in data]) if x]
    exists_jobs = [x['name'] for x in Job.objects.values('name')]
    new_jobs = set(jobs) - set(exists_jobs)
    for job_name in new_jobs:
        job = Job(
            name=job_name,
        )
        job.save()
        yield job


def sync_builds(data):
    created_items = []
    updated_items = []
    builds_map = {(x['job_name'], x['number']): x for x in data}
    builds = [x for x in set([(x['job_name'], x['number']) for x in data]) if x[0] and x[1]]
    exists_builds = [(x['job_id'], x['number']) for x in Build.objects.values('job_id', 'number')]
    new_builds = set(builds) - set(exists_builds)
    for job_name, build_number in new_builds:
        build_data = builds_map[(job_name, build_number)]
        repo_name = build_data['repo']
        branch_name = build_data['branch']
        branch = None
        if repo_name and branch_name:
            branch = Branch.objects.get(repo_id=repo_name, name=branch_name)
        build = Build(
            job_id=job_name,
            number=build_number,
            url=build_data['url'],
            is_building=build_data['is_building'],
            status=build_data['status'],
            revision=build_data['revision'],
            node_id=build_data['node'],
            repo_id=repo_name,
            branch=branch,
            juser_id=build_data['juser'],
        )
        build.save()
        created_items.append(build)

    builds = [x for x in set([(x['job_name'], x['number'], x['state_hash']) for x in data]) if x[0] and x[1]]
    exists_builds = [(x['job_id'], x['number'], x['state_hash']) for x in
                     Build.objects.values('job_id', 'number', 'state_hash')]
    changed_builds = set(builds) - set(exists_builds)

    for job_name, build_number, _ in changed_builds:
        build = Build.objects.get(job_id=job_name, number=build_number)
        build_data = builds_map[(job_name, build_number)]
        repo_name = build_data['repo']
        branch_name = build_data['branch']
        branch = None
        if repo_name and branch_name:
            branch = Branch.objects.get(repo_id=repo_name, name=branch_name)
        build.job_id = job_name
        build.number = build_number
        build.url = build_data['url']
        build.build_at = build_data['build_at']
        build.is_building = build_data['is_building']
        build.status = build_data['status']
        build.revision = build_data['revision']
        build.node_id = build_data['node']
        build.repo_id = repo_name
        build.branch = branch
        build.juser_id = build_data['juser']
        build.save()
        updated_items.append(build)

    return created_items, updated_items


def prepare_result_item(build):
    timestamp = int(build['timestamp']) / 1000
    action_res = {}
    for action in build.get('actions', []):
        for k, v in action.items():
            if k not in action_res:
                action_res[k] = v
    repo = None
    branch = None
    revision = None
    juser = None
    repos = action_res.get('remoteUrls', [])
    if repos:
        repo = repos[0]
    last_build_rev = action_res.get('lastBuiltRevision', {})
    if last_build_rev:
        revision = last_build_rev.get('SHA1')
        branches = last_build_rev.get('branch')
        if branches:
            branch_data = branches[0]
            branch = branch_data.get('name')
    causes = action_res.get('causes', [])
    if causes:
        cause = causes[0]
        juser = cause.get('userName')
    res = {
        'url': build['url'],
        'build_at': datetime.datetime.fromtimestamp(timestamp),
        'job_name': build['job_name'],
        'number': build['number'],
        'is_building': build['building'],
        'node': build['builtOn'],
        'status': build['result'],
        'repo': repo,
        'branch': branch,
        'revision': revision,
        'juser': juser,
    }
    res['state_hash'] = hashlib.md5(json.dumps(res, sort_keys=True, default=json_defaults)).hexdigest()
    return res


def prepare_result_data(jobs):
    builds = reduce(lambda a, b: a + b, [[dict(
        [('job_name', j['name']), ] + b.items()
    ) for b in j['builds']] for j in jobs])
    return map(prepare_result_item, builds)


def fetch():
    load = Load()
    load.save()

    url = jenkins.python_api_url(jenkins.baseurl)

    tree_obj = {
        'jobs': {
            'name': {},
            'builds': {
                'url': {},
                'number': {},
                'timestamp': {},
                'building': {},
                'builtOn': {},
                'result': {},
                'actions': {
                    'causes': {
                        'userName': {},
                    },
                    'remoteUrls': {},
                    'lastBuiltRevision': {
                        'SHA1': {},
                        'branch': {
                            'name': {},
                        },
                    },
                },
            },
        },
    }

    bt = lambda n: u','.join(['{}[{}]'.format(k, bt(v)) if v else k for k, v in n.items()])

    res = jenkins.get_data(url, tree=bt(tree_obj))
    res = prepare_result_data(res['jobs'])

    created_items = []
    updated_items = []

    created_items.extend(list(sync_jusers(res)))
    created_items.extend(list(sync_nodes(res)))
    created_items.extend(list(sync_repos(res)))
    created_items.extend(list(sync_branches(res)))
    created_items.extend(list(sync_jobs(res)))
    created_builds, updated_builds = sync_builds(res)
    created_items.extend(created_builds)
    updated_items.extend(updated_builds)

    for created_item in created_items:
        load_item = LoadItem(
            load=load,
            load_type='create',
            content_object=created_item,
        )
        load_item.save()

    for created_item in updated_items:
        load_item = LoadItem(
            load=load,
            load_type='update',
            content_object=created_item,
        )
        load_item.save()

    if created_items or updated_items:
        load.empty = False
        load.save()


def fetch_tests():
    builds = Build.objects.filter(tests_fetched=False)
    for build in builds:
        result_url = FakeBuild(build.url).get_result_url()
        try:
            results = SimpleResultSet(result_url)
        except HTTPError:
            results = None
        if results:
            for k, r in results.items():
                test = Test(
                    build=build,
                    name=k,
                    status=r.status,
                )
                test.save()
            build.tests_fetched = True
        else:
            hours_from_create = (timezone.now() - build.created_at).seconds / 60 / 60
            if hours_from_create > 5:
                build.tests_fetched = True
        build.save()
