from django.core.management.base import BaseCommand

from core.fetch import fetch, fetch_tests


class Command(BaseCommand):
    def handle(self, **options):
        fetch()
        fetch_tests()
