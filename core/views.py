import math
from itertools import groupby

from django.views.generic import ListView, TemplateView
from django.http import Http404

from models import Repo, Job, Build, Test


class JobListView(ListView):
    model = Job
    template_name = 'job_list.html'

    def dispatch(self, request, *args, **kwargs):
        self.name_search = request.GET.get('name_search', None)
        return super(JobListView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super(JobListView, self).get_queryset()
        if self.name_search:
            queryset = queryset.filter(name__icontains=self.name_search)
        return queryset

    def get_context_data(self, **kwargs):
        context_data = super(JobListView, self).get_context_data(**kwargs)
        context_data['repos'] = Repo.objects.all()
        return context_data


class JobView(ListView):
    model = Build
    template_name = 'job.html'

    def dispatch(self, request, *args, **kwargs):
        self.job = Job.objects.get(pk=kwargs['name'])
        return super(JobView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super(JobView, self).get_queryset()
        return queryset.filter(job_id=self.kwargs['name'])

    def get_context_data(self, **kwargs):
        context_data = super(JobView, self).get_context_data(**kwargs)
        context_data['job'] = self.job
        return context_data


class BuildTestsView(TemplateView):
    diff = False
    last_cnt = 5

    template_name = 'build_tests.html'

    def dispatch(self, request, *args, **kwargs):
        self.job = Job.objects.get(pk=kwargs['name'])
        self.build = Build.objects.get(job=self.job, number=kwargs['number'])
        self.tests = Test.objects.filter(build=self.build)
        self.def_tests = []
        self.def_builds_numbers = []
        if self.diff:
            def_job = self.build.repo.def_job
            last_def_builds = Build.objects.filter(job=def_job, tests_fetched=True) \
                .order_by('-build_at')[:self.last_cnt]

            def_build_tests_maps = {}

            for def_build in last_def_builds:
                self.def_builds_numbers.append(def_build.number)
                def_build_tests = Test.objects.filter(build=def_build)
                def_build_tests_maps[def_build.number] = {x.name: x for x in def_build_tests}

            for test in self.tests:
                test.def_tests = []
                for n in self.def_builds_numbers:
                    test.def_tests.append(def_build_tests_maps.get(n, {}).get(test.name))

            self.tests = [t for t in self.tests if set([t.status]) != set(filter(bool, [getattr(dt, 'status', None) for dt in t.def_tests])) if t.status == 'FAILED']

        return super(BuildTestsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(BuildTestsView, self).get_context_data(**kwargs)
        context_data['diff'] = self.diff
        context_data['job'] = self.job
        context_data['build'] = self.build
        context_data['tests'] = self.tests
        context_data['def_builds_numbers'] = self.def_builds_numbers
        return context_data


class JobTestsView(TemplateView):
    template_name = 'job_tests.html'

    pagination_options = (
        ('10', u'by 10'),
        ('20', u'by 20'),
        ('all', u'All'),
    )

    def dispatch(self, request, *args, **kwargs):
        self.page = int(kwargs.get('page', 1))
        if kwargs.get('size') == 'all':
            self.page_size = 'all'
        else:
            self.page_size = int(kwargs.get('size', 10))
        self.job = Job.objects.get(pk=kwargs['name'])
        self.builds = Build.objects.filter(job=self.job).order_by('-number')
        self.builds_cnt = len(self.builds)
        if self.page_size != 'all':
            self.builds = self.builds[(self.page - 1) * self.page_size:self.page * self.page_size]

        return super(JobTestsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(JobTestsView, self).get_context_data(**kwargs)

        context_data['page'] = self.page

        if self.page_size == 'all':
            context_data['pages'] = []
        else:
            context_data['pages'] = range(1, int(math.ceil(float(self.builds_cnt) / self.page_size)) + 1)

        context_data['page_size'] = unicode(self.page_size)

        context_data['builds_cnt'] = self.builds_cnt

        context_data['job'] = self.job
        context_data['builds'] = self.builds
        tests = Test.objects.filter(build__in=self.builds).values('build', 'name', 'status')

        tests_names = sorted(set([x['name'] for x in tests]))

        tests_sorted = sorted(tests, key=lambda x: (x['build'], x['name']))
        tests_maps = {x[0]: list(x[1])[0]['status'] for x in groupby(tests_sorted, key=lambda x: (x['build'], x['name']))}

        tests_results = [(n, [(b, tests_maps.get((b.id, n))) for b in self.builds]) for n in tests_names]

        srt_by_fails = lambda tr: sum([1 for n, (_, s) in enumerate(tr[1]) if s == 'FAILED'])

        tests_results = sorted(tests_results, key=srt_by_fails, reverse=True)

        context_data['tests_results'] = tests_results

        context_data['pagination_options'] = self.pagination_options
        return context_data



class ReposListView(ListView):
    model = Repo
    template_name = 'repos_list.html'


class RepoTestsView(TemplateView):
    template_name = 'repo_tests.html'

    def dispatch(self, request, *args, **kwargs):
        self.name_search = request.GET.get('name_search', None)
        repos = [x for x in Repo.objects.all() if x.display_name == kwargs['name']]
        if not repos:
            raise Http404

        self.repo = repos[0]

        self.tests = self.repo.get_unique_tests()

        if self.name_search:
            self.tests = [x for x in self.tests if self.name_search.lower() in x.lower()]

        return super(RepoTestsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(RepoTestsView, self).get_context_data(**kwargs)
        context_data['repo'] = self.repo
        context_data['tests'] = self.tests
        return context_data


class TestPassagesView(TemplateView):
    template_name = 'test_passages.html'

    def dispatch(self, request, *args, **kwargs):
        self.test = kwargs['test']
        repos = [x for x in Repo.objects.all() if x.display_name == kwargs['name']]
        if not repos:
            raise Http404

        self.repo = repos[0]
        self.tests = Test.objects.filter(build__repo=self.repo, name=self.test).order_by('build__build_at')
        return super(TestPassagesView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(TestPassagesView, self).get_context_data(**kwargs)
        context_data['repo'] = self.repo
        context_data['test'] = self.test
        context_data['tests'] = self.tests
        return context_data
