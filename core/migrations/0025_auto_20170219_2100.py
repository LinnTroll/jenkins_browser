# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-19 21:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0024_build_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=500)),
                ('status', models.CharField(choices=[(b'FIXED', b'FIXED'), (b'PASSED', b'PASSED'), (b'FAILURE', b'FAILURE'), (b'FAILED', b'FAILED'), (b'SKIPPED', b'SKIPPED')], max_length=32)),
            ],
        ),
        migrations.AlterField(
            model_name='build',
            name='status',
            field=models.CharField(blank=True, choices=[(b'FAIL', b'FAIL'), (b'ERROR', b'ERROR'), (b'ABORTED', b'ABORTED'), (b'REGRESSION', b'REGRESSION'), (b'SUCCESS', b'SUCCESS')], max_length=32, null=True),
        ),
        migrations.AddField(
            model_name='test',
            name='build',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Build'),
        ),
    ]
